// The only function in this file, fizzBuzz is to
// find the sum of all the multiples of 3 or 5 below a given number.

package main

func fizzBuzz(x int) int {
	// We need to subtract one from the number we're analyzing so that we don't include that number
	y := x - 1
	var divisibleSet []int
	for i := y; i > 0; i-- {
		threeDivisible := i % 3
		fiveDivisible := i % 5
		if threeDivisible == 0 || fiveDivisible == 0 {
			divisibleSet = append(divisibleSet, i)
		}
	}
	totalSum := 0
	for _, item := range divisibleSet {
		totalSum += item
	}
	return totalSum
}
