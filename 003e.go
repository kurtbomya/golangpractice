// Function which finds the largest prime factor

package main

import (
	"slices"
)

// Considering breaking out this piece to its own function for readability
//func primerNumberLocator(x int) []int {
//	// Given integer x, find all prime numbers and return this list
//	var primeSet []int
//
//	return primeSet
//}

func maxPrimeFactor(z int) int {
	var factorSet []int
	// Find all prime numbers in the range
	var primeSet []int
	for i := z; i > 1; i-- {
		dividingNumber := 2
		for {
			// Check through the number by dividing by each possible value
			if i%dividingNumber == 0 {
				break
			}
			// If we're here, then the number passed this iteration of the divisible number, bring it up one and test again
			dividingNumber++
			if dividingNumber > i/2 {
				// The dividing number is now larger than half of the number we're testing, there's no way it can be divisible
				// We found our prime number if we got this far
				primeSet = append(primeSet, i)
				println(primeSet)
				break
			}
		}
	}

	// Given all the prime numbers now, cycle through them to see which ones are factors of our given number
	for v := range primeSet {
		if v%z == 0 {
			factorSet = append(factorSet, v)
		}
	}

	// Return the largest integer in the factorSet
	largestFactor := slices.Max(factorSet)

	return largestFactor
}
