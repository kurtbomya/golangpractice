# Usage

```bash
# To make all functions available in the current directory available to the current compilation use:
go run . # assuming you're in the proper directory

# To run only the current file use:
go run 002e.go
```

### Useful Reference Documents

- Go Doc Comments: https://tip.golang.org/doc/comment
