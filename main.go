// Package main implements challenge routines connected to an online coding challenge that requested not to be named

package main

import "fmt"

func main() {
	// Challenge 1
	// Starting number. User can edit this number to configure this for any problem.
	//fmt.Println(fizzBuzz(x))

	// Challenge 2
	//fibSumBelowFourMil()

	// Challenge 3
	fmt.Println(maxPrimeFactor(600851475143))
}
