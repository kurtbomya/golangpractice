// Single function, fibSumBelowFourMil returns the sum of all even Fibonacci sequences below 4 million

package main

import "fmt"

func fibSumBelowFourMil() int {
	// Generate Fibonacci sequence list
	var fibonacciSet []int
	var fibonacciEvenSet []int
	y := 1
	x := 2

	fibonacciSet = append(fibonacciSet, y)
	fibonacciSet = append(fibonacciSet, x)

	for fibonacciSet[len(fibonacciSet)-1] < 4000000 {
		lastEntry := fibonacciSet[len(fibonacciSet)-1]
		secondToLastEntry := fibonacciSet[len(fibonacciSet)-2]
		fibonacciSet = append(fibonacciSet, lastEntry+secondToLastEntry)
	}

	fmt.Println(fibonacciSet)

	for _, v := range fibonacciSet {
		if v % 2 == 0 {
			fibonacciEvenSet = append(fibonacciEvenSet, v)
		}
	}

	totalSum := 0
	for _, item := range fibonacciEvenSet {
		totalSum += item
	}
	fmt.Println(fibonacciEvenSet)
	fmt.Println(totalSum)
	return totalSum
}
